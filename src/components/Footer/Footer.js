import React from "react";
import { Link, BottomNavigation } from "@material-ui/core";
import "./Footer.css";

export default function Footer() {
  return (
    <BottomNavigation id="btm-nav">
      <Link href="https://www.bambu.life/">
        <span role="img" aria-label="lightning">
          ⚡
        </span>
        POWERED BY BAMBU
      </Link>
    </BottomNavigation>
  );
}
