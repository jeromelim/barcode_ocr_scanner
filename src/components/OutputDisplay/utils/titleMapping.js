const titleMap = {
  cardNo: "Card No.",
  dateOfBirth: "Date of Birth",
  dateOfExpiry: "Date of Expiry",
  dateOfIssue: "Date of Issue",
  id: "ID",
  name: "Name",
  surname: "Surname",
  thaiAdress: "Thai Address",
  thaiNameSurname: "Thai Name/Surname",
  barcode_id: "Barcode ID.",
  data_of_birth_th: "Date of Birth (Thai)",
  data_of_birth_en: "Date of Birth",
  thai_address: "Thai Address",
  date_of_issue: "Date of Issue",
  date_of_expiry: "Date of Expiry",
  id_card: "ID Card"
};

module.exports = { titleMap };
