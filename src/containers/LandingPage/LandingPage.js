import React from "react";
import { Button, Typography } from "@material-ui/core";
import { NavLink as Link } from "react-router-dom";
import "./LandingPage.css";
import NavBar from "../../components/NavBar/NavBar";
import { ReactComponent as BarCode } from "../assets/barcode-solid.svg";
import { ReactComponent as SearchIcon } from "../assets/search-solid.svg";

const LandingPage = () => {
  return (
    <div>
      <NavBar />
      <div className="btn-group">
        <div>
          <Button variant="contained" color="primary">
            <Link to="/barcode">
              <Typography variant="h5">
                <BarCode />
                Barcode
              </Typography>
            </Link>
          </Button>
        </div>
        <div>
          <Button variant="contained" color="primary">
            <Link to="/ocr">
              <Typography variant="h5" align="center">
                <SearchIcon />
                <br />
                OCR
              </Typography>
            </Link>
          </Button>
        </div>
      </div>
    </div>
  );
};

export default LandingPage;
