import React, { Component } from "react";
import axios from "axios";

import { FilePond, registerPlugin } from "react-filepond";
import "filepond/dist/filepond.min.css";
import FilePondPluginImageExifOrientation from "filepond-plugin-image-exif-orientation";
import FilePondPluginImagePreview from "filepond-plugin-image-preview";
import FilePondPluginFileEncode from "filepond-plugin-file-encode";
import "filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css";

import OutputDisplay from "./OutputDisplay/OutputDisplay";
import { Typography, LinearProgress } from "@material-ui/core";

registerPlugin(
  FilePondPluginImageExifOrientation,
  FilePondPluginImagePreview,
  FilePondPluginFileEncode
);

class Upload extends Component {
  constructor(props) {
    super(props);

    this.state = {
      files: [],
      base64: "",
      picData: "",
      isFetching: false
    };
  }
  handleConversion = async () => {
    this.setState({ base64: this.pond.getFile().getFileEncodeBase64String() });
    const processData = await this.uploadBase64(this.state.base64);
    this.setState({
      picData: processData
    });
  };

  uploadBase64 = async input => {
    this.setState({ isFetching: true });
    try {
      const data = await axios.post(
        `http://165.22.59.79:5000/${this.props.api}`,
        {
          data: input
        }
      );
      await this.setState({ isFetching: false });
      return data;
    } catch (e) {
      throw new Error(e);
    }
  };

  render() {
    const { picData, files, isFetching } = this.state;
    const { api } = this.props;
    const { handleConversion } = this;
    return (
      <React.Fragment>
        <FilePond
          ref={ref => (this.pond = ref)}
          files={files}
          allowMultiple={false}
          server="/api"
          onaddfile={handleConversion}
          instantUpload={false}
          onupdatefiles={fileItems => {
            this.setState({
              files: fileItems.map(fileItem => fileItem.file)
            });
          }}
        />
        {isFetching ? (
          <>
            <Typography variant="h5" align="center">
              Please be patient, analysing image...
              <LinearProgress />
            </Typography>
          </>
        ) : (
          undefined
        )}
        {picData ? <OutputDisplay output={picData} api={api} /> : undefined}
      </React.Fragment>
    );
  }
}

export default Upload;
