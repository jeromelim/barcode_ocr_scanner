import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import BackIcon from "@material-ui/icons/ArrowBack";
import CrossIcon from "@material-ui/icons/Close";

class NavBar extends Component {
  constructor(props) {
    super(props);
    this.state = { redirect: false };
  }

  handleCloseWindow = () => {
    window.open("about:blank", "_self");
    window.close();
  };

  handleBackButton = () => {
    this.setState({ redirect: true });
  };

  render() {
    const { path } = this.props;
    return (
      <div>
        {this.state.redirect ? <Redirect push to="/" /> : undefined}
        <AppBar position="static">
          <Toolbar>
            {path ? (
              <IconButton
                edge="start"
                color="inherit"
                onClick={this.handleBackButton}
              >
                <BackIcon />
              </IconButton>
            ) : (
              <IconButton
                edge="start"
                color="inherit"
                onClick={this.handleCloseWindow}
              >
                <CrossIcon />
              </IconButton>
            )}
            <Typography variant="h5">
              {path === "ocr"
                ? "OCR Tool"
                : path === "barcode"
                ? "Barcode Tool"
                : "Scanner Tool"}
            </Typography>
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

export default NavBar;
