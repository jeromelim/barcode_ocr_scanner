import React from "react";
import {
  Card,
  Table,
  TableBody,
  TableRow,
  TableCell,
  AppBar,
  Typography,
  Toolbar
} from "@material-ui/core";
import { titleMap } from "./utils/titleMapping";

const OutputDisplay = ({ output, api }) => {
  let arr = output.data
    .replace("{", "")
    .replace("}", "")
    .split(",")
    .map(a => a.split(":"));

  return (
    <Card className="data-display">
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6">Results</Typography>
        </Toolbar>
      </AppBar>
      <Table>
        <TableBody>
          {arr.map((row, index) => (
            <TableRow key={index}>
              <TableCell>
                <strong>{titleMap[row[0].trim()]}</strong>
              </TableCell>
              <TableCell>{row[1]}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </Card>
  );
};

export default OutputDisplay;
