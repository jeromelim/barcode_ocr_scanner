import React from "react";
import Upload from "../components/Upload";
import NavBar from "../components/NavBar/NavBar";

const OCRPage = ({ path }) => {
  return (
    <React.Fragment>
      <NavBar path={path} />
      <Upload api={"ocr"} />
    </React.Fragment>
  );
};

export default OCRPage;
