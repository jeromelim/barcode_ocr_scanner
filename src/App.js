import React from "react";
import { Switch, Route } from "react-router-dom";
import { Container, Paper } from "@material-ui/core";
import BarCodePage from "./containers/BarCodePage";
import OCRPage from "./containers/OCRPage";
import LandingPage from "./containers/LandingPage/LandingPage";
import Footer from "./components/Footer/Footer";

function App() {
  return (
    <>
      <Container component="main" maxWidth="sm">
        <Paper>
          <Switch>
            <Route path="/" exact component={LandingPage} />
            <Route
              path="/barcode"
              component={() => <BarCodePage path={"barcode"} />}
            />
            <Route path="/ocr" component={() => <OCRPage path={"ocr"} />} />
          </Switch>
          <Footer />
        </Paper>
      </Container>
    </>
  );
}

export default App;
