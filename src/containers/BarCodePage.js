import React from "react";

import Upload from "../components/Upload";
import NavBar from "../components/NavBar/NavBar";

const BarCodePage = ({ path }) => {
  return (
    <React.Fragment>
      <NavBar path={path} />
      <Upload api={"barcode"} />
    </React.Fragment>
  );
};

export default BarCodePage;
